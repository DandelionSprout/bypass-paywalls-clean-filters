// ==UserScript==
// @name            Bypass Paywalls Clean - en
// @version         2.7.7.4
// @downloadURL     https://gitlab.com/magnolia1234/bypass-paywalls-clean-filters/-/raw/main/userscript/bpc.en.user.js
// @updateURL       https://gitlab.com/magnolia1234/bypass-paywalls-clean-filters/-/raw/main/userscript/bpc.en.user.js
// @match           *://*.com/*
// @match           *://*.co.uk/*
// @match           *://*.com.au/*
// @match           *://*.net.au/*
// @match           *://*.org/*
// @match           *://*.independent.ie/*
// @match           *://*.indiatoday.in/*
// @match           *://*.ipolitics.ca/*
// @match           *://*.livelaw.in/*
// @match           *://*.nation.africa/*
// @match           *://*.niagarafallsreview.ca/*
// @match           *://*.nzherald.co.nz/*
// @match           *://*.stcatharinesstandard.ca/*
// @match           *://*.wellandtribune.ca/*
// ==/UserScript==

(function() {
  'use strict';

if (matchDomain('nzherald.co.nz')) {
  function nzherald_main() {
    if (window.Fusion)
      window.Fusion.globalContent.isPremium = false;
  }
  window.setTimeout(function () {
    insert_script(nzherald_main);
  }, 100);
}

window.setTimeout(function () {

var ca_torstar_domains = ['niagarafallsreview.ca', 'stcatharinesstandard.ca', 'thepeterboroughexaminer.com', 'therecord.com', 'thespec.com', 'thestar.com', 'wellandtribune.ca'];
var no_nhst_media_domains = ['intrafish.com', 'rechargenews.com', 'tradewindsnews.com', 'upstreamonline.com'];
var usa_adv_local_domains = ['al.com', 'cleveland.com', 'lehighvalleylive.com', 'masslive.com', 'mlive.com', 'nj.com', 'oregonlive.com', 'pennlive.com', 'silive.com', 'syracuse.com'];
var usa_conde_nast_domains = ['architecturaldigest.com', 'bonappetit.com', 'gq.com' , 'newyorker.com', 'vanityfair.com', 'vogue.com', 'wired.com'];
var usa_craincomm_domains = ['adage.com', 'autonews.com', 'chicagobusiness.com', 'crainscleveland.com', 'crainsdetroit.com', 'crainsnewyork.com', 'modernhealthcare.com'];
var usa_hearst_comm_domains = ['expressnews.com', 'houstonchronicle.com', 'sfchronicle.com'];
var domain;
var usa_lee_ent_domains = ['buffalonews.com', 'richmond.com', 'tucson.com', 'tulsaworld.com'];
var usa_mcc_domains = ['bnd.com', 'charlotteobserver.com', 'fresnobee.com', 'kansas.com', 'kansascity.com', 'kentucky.com', 'miamiherald.com', 'newsobserver.com', 'sacbee.com', 'star-telegram.com', 'thestate.com', 'tri-cityherald.com'];
var usa_mng_domains =   ['denverpost.com', 'eastbaytimes.com', 'mercurynews.com', 'ocregister.com', 'pe.com', 'twincities.com'];
var usa_outside_mag_domains = ["backpacker.com", "betamtb.com", "betternutrition.com", "cleaneatingmag.com", "climbing.com", "cyclingtips.com", "gymclimber.com", "outsideonline.com", "oxygenmag.com", "pelotonmagazine.com", "podiumrunner.com", "rockandice.com", "skimag.com", "trailrunnermag.com", "triathlete.com", "vegetariantimes.com", "velonews.com", "womensrunning.com", "yogajournal.com"];
var usa_tribune_domains = ['baltimoresun.com', 'chicagotribune.com', 'courant.com', 'dailypress.com', 'mcall.com', 'nydailynews.com', 'orlandosentinel.com', 'pilotonline.com', 'sun-sentinel.com'];

if (window.location.hostname.match(/\.(com|net)\.au$/)) {//australia

if (matchDomain('thesaturdaypaper.com.au')) {
  let paywall = document.querySelector('div.paywall-hard-always-show');
  removeDOMElement(paywall);
}

else if (matchDomain(['brisbanetimes.com.au', 'smh.com.au', 'theage.com.au', 'watoday.com.au'])) {
  if (!window.location.hostname.startsWith('amp.')) {
    let paywall = document.querySelector('meta[content^="FOR SUBSCRIBERS"], #paywall_prompt');
    let amphtml = document.querySelector('link[rel="amphtml"]');
    if (paywall && amphtml) {
      removeDOMElement(paywall);
      window.location.href = amphtml.href;
    }
  } else {
    amp_unhide_subscr_section();
  }
}

else {
  // Australian Community Media newspapers
  let au_comm_media_domains = ['bendigoadvertiser.com.au', 'bordermail.com.au', 'canberratimes.com.au', 'centralwesterndaily.com.au', 'dailyadvertiser.com.au', 'dailyliberal.com.au', 'examiner.com.au', 'illawarramercury.com.au', 'newcastleherald.com.au', 'northerndailyleader.com.au', 'standard.net.au', 'theadvocate.com.au', 'thecourier.com.au', 'westernadvocate.com.au'];
  if (matchDomain(au_comm_media_domains)) {
    let mask = document.querySelector('div[style^="-webkit-mask-image"]');
    if (mask) {
      mask.removeAttribute('style');
      let div_hidden = document.querySelectorAll('div[class="hidden"]');
      for (let elem of div_hidden)
        elem.classList.remove('hidden');
    } else {
      let subscribe_truncate = document.querySelector('.subscribe-truncate');
      if (subscribe_truncate)
        subscribe_truncate.classList.remove('subscribe-truncate');
      let subscriber_hiders = document.querySelectorAll('.subscriber-hider');
      for (let subscriber_hider of subscriber_hiders)
        subscriber_hider.classList.remove('subscriber-hider');
    }
    let blocker = document.querySelector('div.blocker');
    let noscroll = document.querySelector('body[style]');
    if (noscroll)
      noscroll.removeAttribute('style');
    let story_generic_iframe = document.querySelector('.story-generic__iframe');
    let ads = document.querySelectorAll('.ad-placeholder, .sticky, [id*="-container"]');
    removeDOMElement(story_generic_iframe, blocker, ...ads);
  } else if (window.location.hostname.endsWith('.com.au')) {
    // Australia News Corp
    let au_news_corp_domains = ['adelaidenow.com.au', 'cairnspost.com.au', 'codesports.com.au', 'couriermail.com.au', 'dailytelegraph.com.au', 'geelongadvertiser.com.au', 'goldcoastbulletin.com.au', 'heraldsun.com.au', 'ntnews.com.au', 'theaustralian.com.au', 'thechronicle.com.au', 'themercury.com.au', 'townsvillebulletin.com.au', 'weeklytimesnow.com.au'];
    if (matchDomain(au_news_corp_domains)) {
      let url = window.location.href;
      if (url.includes('/subscribe/')) {
        if (!url.includes('/digitalprinteditions') && url.includes('dest=') && url.split('dest=')[1].split('&')[0]) {
          let url_new = decodeURIComponent(url.split('dest=')[1].split('&')[0]);
          if (!matchDomain('theaustralian.com.au'))
            url_new += '?amp';
          else
            url_new = url_new.replace('www.', 'amp.');
          window.setTimeout(function () {
            window.location.href = url_new;
          }, 500);
        }
      } else {
        let header_ads = document.querySelector('.header_ads-container');
        removeDOMElement(header_ads);
        let amp_ads_sel = 'amp-ad, amp-embed, [id^="ad-mrec-"], .story-ad-container';
        let comments;
        if (window.location.hostname.startsWith('amp.')) {
          amp_unhide_subscr_section(amp_ads_sel, true, true, 'resourcesssl.newscdn.com.au');
          comments = document.querySelector('#story-comments, .comments-wrapper');
        } else if (window.location.search.match(/(\?|&)amp/)) {
          amp_unhide_subscr_section(amp_ads_sel, true, true, 'resourcesssl.newscdn.com.au');
          comments = document.querySelector('#comments-load');
          let amp_iframe_sizers = document.querySelectorAll('amp-iframe > i-amphtml-sizer');
          removeDOMElement(...amp_iframe_sizers)
        }
        removeDOMElement(comments);
      }
    } else {
      // Australian Seven West Media
      if (matchDomain('thewest.com.au')) {
        window.setTimeout(function () {
          let breach_screen = document.querySelector('div[data-testid*="BreachScreen"]');
          if (breach_screen) {
            let scripts = document.querySelectorAll('script:not([src], [type])');
            let json_script;
            for (let script of scripts) {
              if (script.innerText.includes('window.PAGE_DATA =')) {
                json_script = script;
                break;
              }
            }
            if (json_script) {
              let json_text = json_script.innerHTML.split('window.PAGE_DATA =')[1].split('</script')[0];
              json_text = json_text.replace(/undefined/g, '"undefined"');
              let json_article = JSON.parse(json_text);
              let json_pub;
              for (let key in json_article)
                if (json_article[key].data.result.resolution && json_article[key].data.result.resolution.publication) {
                  json_pub = json_article[key].data.result.resolution.publication;
                  break;
                }
              let json_content = [];
              let url_loaded;
              if (json_pub) {
                json_content = json_pub.content.blocks;
                url_loaded = json_pub._self;
              } else
                window.location.reload(true);
              //let json_video = json_pub.mainVideo;
              let url = window.location.href;
              if (!url_loaded || !url.includes(url_loaded.slice(-10)))
                window.location.reload(true);
              let par_elem, par_sub1, par_sub2;
              let par_dom = document.createElement('div');
              let tweet_id = 1;
              for (let par of json_content) {
                par_elem = '';
                if (par.kind === 'text') {
                  par_elem = document.createElement('p');
                  par_elem.innerText = par.text;
                } else if (par.kind === 'subhead') {
                  par_elem = document.createElement('h2');
                  par_elem.innerText = par.text;
                } else if (par.kind === 'pull-quote') {
                  par_elem = document.createElement('i');
                  par_elem.innerText = (par.attribution ? par.attribution + ': ' : '') + par.text;
                } else if (par.kind === 'embed') {
                  if (par.reference.includes('https://omny.fm/') || par.reference.includes('https://docdro.id/')) {
                    par_elem = document.createElement('embed');
                    par_elem.src = par.reference;
                    par_elem.style = 'height:500px; width:100%';
                    par_elem.frameborder = '0';
                  } else {
                    par_elem = document.createElement('a');
                    par_elem.href = par.reference;
                    par_elem.innerText = par.reference.split('?')[0];
                    console.log('embed: ' + par.reference);
                  }
                } else if (par.kind === 'unordered-list') {
                  if (par.items) {
                    par_elem = document.createElement('ul');
                    for (let item of par.items)
                      if (item.text) {
                        par_sub1 = document.createElement('li');
                        if (item.intentions[0] && item.intentions[0].href) {
                          par_sub2 = document.createElement('a');
                          par_sub2.href = item.intentions[0].href;
                        } else {
                          par_sub2 = document.createElement('span');
                        }
                        par_sub2.innerText = item.text;
                        par_sub1.appendChild(par_sub2);
                        par_elem.appendChild(par_sub1);
                      }
                  }
                } else if (par.kind === 'inline') {
                  if (par.asset.kind === 'image') {
                    par_elem = document.createElement('figure');
                    par_sub1 = document.createElement('img');
                    par_sub1.src = par.asset.original.reference;
                    par_sub1.style = 'width:100%';
                    par_elem.appendChild(par_sub1);
                    if (par.asset.captionText) {
                      par_sub2 = document.createElement('figcaption');
                      par_sub2.innerText = par.asset.captionText + ' ' + par.asset.copyrightByline +
                        ((par.asset.copyrightCredit && par.asset.captionText !== par.asset.copyrightByline) ? '/' + par.asset.copyrightCredit : '');
                      par_elem.appendChild(par_sub2);
                    }
                  }
                } else {
                  par_elem = document.createElement('p');
                  par_elem.innerText = par.text;
                  console.log(par.kind);
                }
                if (par_elem)
                  par_dom.appendChild(par_elem);
              }
              let content = document.querySelector('div[class*="StyledArticleContent"]');
              if (content) {
                content.appendChild(par_dom);
              } else {
                par_dom.setAttribute('style', 'margin: 20px;');
                breach_screen.parentElement.insertBefore(par_dom, breach_screen);
              }
            }
            removeDOMElement(breach_screen);
          }
        }, 1500);
        let header_advert = document.querySelector('.headerAdvertisement');
        if (header_advert)
          header_advert.setAttribute('style', 'display: none;');
      }
    }
  }
}

} else if (window.location.hostname.match(/\.(ie|uk)$/)) {//united kingdom/ireland

if (domain = matchDomain(['belfasttelegraph.co.uk', 'independent.ie'])) {
  if (!cookieExists('subscriber'))
    setCookie('subscriber', '{"subscriptionStatus": true}', domain, '/', 14);
}

else if (matchDomain('independent.co.uk')) {
  let url = window.location.href;
  if (window.location.search.match(/(\?|&)amp/)) {
    let ads = document.querySelectorAll('amp-ad, amp-embed, [id^="ad-"]');
    removeDOMElement(...ads);
  } else {
    let paywall = document.querySelector('div.article-premium');
    let related = document.querySelector('div.related');
    let msg = document.querySelector('div#bpc_archive');
    if (paywall && !related && !msg) {
      paywall.classList.remove('article-premium');
      let article = document.querySelector('div#main');
      if (article)
        article.insertBefore(archiveLink(url), article.firstChild);
    }
  }
}

else if (matchDomain('prospectmagazine.co.uk')) {
  let url = window.location.href;
  let paywall = document.querySelector('div.paywall_overlay_blend, div.paywall');
  if (paywall) {
    removeDOMElement(paywall);
    let url_cache = 'https://webcache.googleusercontent.com/search?q=cache:' + url;
    replaceDomElementExt(url_cache, true, false, 'main');
  }
}

else if (matchDomain('spectator.co.uk')) {
  if (window.location.pathname.match(/\/amp(\/)?$/)) {
    let banners = document.querySelectorAll('div[amp-access^="p.show"], div[amp-access*="NOT loggedIn"]');
    removeDOMElement(...banners);
  } else if (window.location.pathname.startsWith('/article/')) {
    let paywall = document.querySelector('.HardPayWallContainer-module__overlay');
    let body_par = document.querySelector('p[class^="ContentPageBodyParagraph"]');
    let amphtml = document.querySelector('link[rel="amphtml"]');
    if ((paywall || !body_par) && amphtml) {
      removeDOMElement(paywall);
      window.setTimeout(function () {
        window.location.href = amphtml.href;
      }, 500);
    }
  }
}

else if (matchDomain('telegraph.co.uk')) {
  let url = window.location.href.split('?')[0];
  if (url.endsWith('/amp/')) {
    let paywall = document.querySelectorAll('.premium-paywall');
    if (paywall.length) {
      let truncated_content = document.querySelector('.truncated-content');
      removeDOMElement(...paywall, truncated_content);
      amp_unhide_access_hide('="c.result=\'ALLOW_ACCESS\'"', '', 'amp-ad, amp-embed', false);
    } else {
      let amp_ads = document.querySelectorAll('amp-ad, amp-embed');
      removeDOMElement(...amp_ads);
    }
  } else {
    let subwall = document.querySelectorAll('[class^="subwall"]');
    let ads = document.querySelectorAll('.advert, .commercial-unit');
    removeDOMElement(...subwall, ...ads);
  }
}

else if (matchDomain('the-tls.co.uk')) {
  let paywall = document.querySelector('.tls-subscriptions-banner__closed-skin');
  removeDOMElement(paywall);
}

else if (matchDomain('thetimes.co.uk')) {
  let url = window.location.href;
  if (window.location.hostname !== 'epaper.thetimes.co.uk') {
    let block = document.querySelector('.subscription-block');
    let adverts = document.querySelectorAll('#ad-article-inline, #sticky-ad-header, div[class*="InlineAdWrapper"], div[class*="NativeAd"], div.gyLkkj');
    removeDOMElement(block, ...adverts);
    let paywall = document.querySelector('div#paywall-portal-article-footer');
    if (paywall && !url.includes('?shareToken=')) {
      removeDOMElement(paywall);
      let article = document.querySelector('article[class^="responsive__BodyContainer"]');
      if (article)
        article.insertBefore(archiveLink(url), article.firstChild);
    }
    let paywall_page = document.querySelector('div#paywall-portal-page-footer');
    removeDOMElement(paywall_page);
  } else {
    if (url.includes('/textview')) {
      let article_locks = document.querySelectorAll('article[aid] > div > div > a.readmore.dis[href="javascript:void(0)"]');
      for (let elem of article_locks) {
        let article = elem.closest('article[aid]');
        if (article)
          elem.href = 'article/' + article.getAttribute('aid');
      }
      let headers = document.querySelectorAll('article[aid] > header > hgroup > h1');
      for (let elem of headers) {
        let article = elem.closest('article[aid]');
        if (article && !elem.innerHTML.includes('</a>')) {
          let new_link = document.createElement('a');
          new_link.href = 'article/' + article.getAttribute('aid');
          new_link.innerText = elem.innerText;
          elem.innerText = '';
          elem.appendChild(new_link);
        }
      }
      let continued_links = document.querySelectorAll('a.continued[href="javascript:void(0)"]');
      for (let elem of continued_links) {
        let pagenr = parseInt(elem.innerText.replace('Continued from ', ''));
        if (pagenr)
          elem.href = url.replace(/\/page\/(\d)+\/textview/, '/page/' + pagenr + '/textview');
      }
      let bottom_links = document.querySelectorAll('article > a[id^="athumb_"][article-id][href="javascript:void(0)"]');
      for (let elem of bottom_links)
        elem.href = 'article/' + elem.getAttribute('article-id');
    } else {
      let pages = document.querySelectorAll('div.page-left, div.page-right');
      for (let page of pages)
        page.style.height = 'auto';
    }
  }
}

} else {

if (matchDomain(usa_adv_local_domains)) {
  let url = window.location.href;
  if (url.includes('?outputType=amp')) {
    let amp_ads = document.querySelectorAll('.amp-ad-container, amp-embed');
    removeDOMElement(...amp_ads);
  } else {
    let paywall = document.querySelector('.paywall');
    let amphtml = document.querySelector('link[rel="amphtml"]');
    if (paywall && amphtml) {
      removeDOMElement(paywall);
      window.location.href = amphtml.href;
    }
    let ads = document.querySelectorAll('div.ad');
    removeDOMElement(...ads);
  }
}

else if (matchDomain('adweek.com')) {
  let url = window.location.href;
  let body_single = document.querySelector('body.single');
  let amphtml = document.querySelector('link[rel="amphtml"]');
  if (body_single && amphtml) {
    body_single.classList.remove('single');
    window.location.href = amphtml.href;
  }
}

else if (matchDomain('americanbanker.com')) {
  let inline_gate = document.querySelector('.inline-gate');
  if (inline_gate) {
    inline_gate.classList.remove('inline-gate');
    let inline_gated = document.querySelectorAll('.inline-gated');
    for (let elem of inline_gated)
      elem.classList.remove('inline-gated');
  }
}

else if (matchDomain('artnet.com')) {
  if (window.location.pathname.endsWith('/amp-page')) {
    amp_unhide_subscr_section();
  } else {
    let body_hidden = document.querySelector('.article-body');
    if (body_hidden)
      body_hidden.style = 'display:block;';
  }
}

else if (matchDomain('asia.nikkei.com')) {
  let popup = document.querySelector('#pianoj_ribbon');
  removeDOMElement(popup);
}

else if (matchDomain('asiatimes.com')) {
  if (!window.location.search.match(/(\?|&)amp_markup=1/)) {
    let paywall = document.querySelector('div.woocommerce');
    if (paywall) {
      removeDOMElement(paywall);
      let url_amp = window.location.href.split('?')[0] + '?amp_markup=1';
      replaceDomElementExt(url_amp, false, false, 'div.entry-content', '', 'article.ia2amp-article');
    }
  }
}

else if (matchDomain('billboard.com')) {
  if (window.location.pathname.endsWith('/amp/')) {
    amp_unhide_subscr_section('amp-ad, amp-embed');
  }
}

else if (matchDomain('barrons.com')) {
  let url = window.location.href;
  if (!url.includes('barrons.com/amp/')) {
    let body_continuous = document.querySelector('body.is-continuous');
    let snippet = document.querySelector('meta[content="snippet"]');
    if (body_continuous && snippet) {
      removeDOMElement(snippet);
      window.location.href = url.replace('barrons.com', 'barrons.com/amp');
    }
    let continue_buttons = document.querySelectorAll('button.snippet__buttons--continue');
    for (let elem of continue_buttons)
      elem.addEventListener('click', function () { window.location.reload(); });
    let barrons_ads = document.querySelectorAll('.barrons-body-ad-placement');
    removeDOMElement(...barrons_ads);
  } else {
    amp_unhide_subscr_section('.wsj-ad, amp-ad');
    let login = document.querySelector('div.login-section-container');
    removeDOMElement(login);
    let amp_images = document.querySelectorAll('amp-img');
    for (let amp_img of amp_images) {
      let img_new = document.createElement('img');
      img_new.src = amp_img.getAttribute('src');
      amp_img.parentNode.replaceChild(img_new, amp_img);
    }
  }
}

else if (matchDomain('bloomberg.com')) {
  setCookie('gatehouse_id', '', 'bloomberg.com', '/', 0);
  sessionStorage.clear();
  function bloomberg_noscroll(node) {
    node.removeAttribute('data-paywall-overlay-status');
  }
  waitDOMElement('div#fortress-paywall-container-root', 'DIV', removeDOMElement, true);
  waitDOMAttribute('body', 'BODY', 'data-paywall-overlay-status', bloomberg_noscroll, true);
  let paywall = document.querySelector('div#fortress-paywall-container-root');
  let counter = document.querySelector('div#fortress-preblocked-container-root');
  let leaderboard = document.querySelector('div[id^="leaderboard"], div[class^="leaderboard"], div.canopy-container');
  let noscroll = document.querySelector('body[data-paywall-overlay-status]');
  if (noscroll)
    noscroll.removeAttribute('data-paywall-overlay-status');
  removeDOMElement(paywall, counter, leaderboard);
  let url = window.location.href;
  if (url.match(/s\/\d{4}-/)) {
    let page_ad = document.querySelectorAll('div.page-ad, div[data-ad-placeholder], div[class*="-ad-top"]');
    let reg_ui_client = document.querySelector('div#reg-ui-client');
    removeDOMElement(leaderboard, ...page_ad, reg_ui_client);
    let hidden_images = document.querySelectorAll('img.lazy-img__image[src][data-native-src]');
    for (let hidden_image of hidden_images) {
      if (hidden_image.src.match(/\/(60|150)x-1\.(png|jpg)$/))
        hidden_image.setAttribute('src', hidden_image.getAttribute('data-native-src'));
      hidden_image.style.filter = 'none';
    }
    let hidden_charts = document.querySelectorAll('div[data-toaster-id][data-src]');
    for (let hidden_chart of hidden_charts) {
      let elem = document.createElement('iframe');
      Object.assign(elem, {
        src: hidden_chart.getAttribute('data-src'),
        frameborder: 0,
        height: hidden_chart.getAttribute('style').replace('min-height: ', ''),
        scrolling: 'no'
      });
      hidden_chart.parentNode.replaceChild(elem, hidden_chart);
    }
    let blur = document.querySelector('div.blur[style]');
    if (blur) {
      blur.classList.remove('blur');
      blur.removeAttribute('style');
    }
    let shimmering_content = document.querySelectorAll('div.shimmering-text');
    let body_transparent = document.querySelector('div[class*="nearly-transparent-text-blur"]');
    if (shimmering_content.length || body_transparent) {
      removeDOMElement(...shimmering_content);
      if (body_transparent)
        removeClassesByPrefix(body_transparent, 'nearly-transparent-text-blur');
      let json_script = document.querySelector('script[data-component-props="ArticleBody"], script[data-component-props="FeatureBody"]');
      if (json_script) {
        let json = JSON.parse(json_script.innerHTML);
        if (json) {
          let json_text;
          if (json.body)
            json_text = json.body;
          else if (json.story && json.story.body)
            json_text = json.story.body;
          if (json_text) {
            removeDOMElement(json_script);
            let article = document.querySelector('div.body-copy-v2:not(.art_done)');
            let article_class = 'body-copy-v2';
            if (!article) {
              article = document.querySelector('div.body-copy:not(.art_done)');
              article_class = 'body-copy';
            }
            if (!article) {
              article = document.querySelector('div.body-content:not(.art_done)');
              article_class = 'body-content';
            }
            if (article) {
              article_class += ' art_done';
              let parser = new DOMParser();
              let doc = parser.parseFromString('<div class="' + article_class + '">' + json_text + '</div>', 'text/html');
              let article_new = doc.querySelector('div');
              if (article_new) {
                article.parentNode.replaceChild(article_new, article);
                let teaser_body = document.querySelector('div.body-content[class*="teaser-content_"]');
                removeDOMElement(teaser_body);
                let thirdparty_embed = document.querySelector('div.thirdparty-embed__container[style*="height: 0;"]');
                if (thirdparty_embed)
                  thirdparty_embed.setAttribute('style', 'height: 550px !important;');
              }
            }
          }
        }
      }
    }
  }
}

else if (matchDomain('bloombergquint.com')) {
  if (window.location.pathname.startsWith('/amp/')) {
    amp_unhide_subscr_section();
  }
}

else if (matchDomain('bostonglobe.com')) {
  if (window.location.search.startsWith('?outputType=amp')) {
    amp_unhide_subscr_section();
  } else {
    let ads = document.querySelectorAll('div.arc_ad');
    removeDOMElement(...ads);
  }
}

else if (matchDomain('businessoffashion.com')) {
  let ads = document.querySelectorAll('div[class^="default__AdsBlockWrapper"]');
  removeDOMElement(...ads);
}

else if (matchDomain(ca_torstar_domains)) {
  let meter_banner = document.querySelector('.c-article-meter-banner');
  let ads = document.querySelectorAll('.seo-media-query, .c-googleadslot');
  removeDOMElement(meter_banner, ...ads);
  let end_of_article = document.querySelector('#end-of-article');
  if (end_of_article)
    end_of_article.setAttribute('style', 'display:none;');
  let rightrail = document.querySelector('.c-article-body__rightrail');
  if (rightrail)
    rightrail.setAttribute('style', 'display:none;');
}

else if (matchDomain('cen.acs.org')) {
  setCookie('paywall-cookie', '', 'cen.acs.org', '/', 0);
  let meteredBar = document.querySelector('.meteredBar');
  removeDOMElement(meteredBar);
}

else if (matchDomain('chronicle.com')) {
  let preview = document.querySelector('div[data-content-summary]');
  removeDOMElement(preview);
  let article_hidden = document.querySelector('div[data-content-body]');
  if (article_hidden)
    article_hidden.removeAttribute('data-content-body');
}

else if (matchDomain('csmonitor.com')) {
  let paywall = document.querySelector('div.paywall');
  removeDOMElement(paywall);
}

else if (matchDomain('dailywire.com')) {
  let paywall = document.querySelector('#post-body-text > div > div[class]');
  if (paywall)
    paywall.removeAttribute('class');
}

else if (matchDomain('dallasnews.com')) {
  if (window.location.search.startsWith('?outputType=amp')) {
    amp_unhide_subscr_section('amp-ad, amp-embed');
  } else {
    let overlay = document.querySelector('div.sl-overlay');
    removeDOMElement(overlay);
    let noscroll = document.querySelector('div#courier-body-wrapper[style]');
    if (noscroll)
      noscroll.removeAttribute('style');
  }
}

else if (matchDomain('digiday.com')) {
  if (window.location.pathname.endsWith('/amp/')) {
    amp_unhide_access_hide('="NOT p.showPageviewExpired AND NOT p.showPayWall"', '', 'amp-ad, .advertisement, .ad-wrapper');
  }
}

else if (matchDomain('discovermagazine.com')) { //cookie
  let mammoth = document.querySelector('.iXVGnF');
  if (mammoth)
    window.location.reload();
  let banner = document.querySelector('div.dPURIw');
  if (banner)
    banner.setAttribute('style', 'display:none;');
}

else if (matchDomain('economictimes.com')) {
  if (window.location.pathname.includes('/amp_')) {
    let paywall = document.querySelector('.paywall_wrap');
    if (paywall) {
      let content = document.querySelector('.paywall[style="display:none;"]');
      if (content)
        content.setAttribute('style', 'display:block;');
      let intro = document.querySelector('.art_wrap');
      let article_blocker = document.querySelector('.articleBlocker');
      let amp_ads = document.querySelectorAll('amp-ad');
      removeDOMElement(paywall, intro, article_blocker, ...amp_ads);
    }
  } else {
    window.setTimeout(function () {
      let paywall = document.querySelector('div#blocker_layer');
      let data_prime = document.querySelector('div[data-prime="1"]');
      if (paywall || data_prime) {
        removeDOMElement(paywall);
        if (data_prime)
          data_prime.removeAttribute('data-prime');
        let content = document.querySelector('div[id^="articlebody_"]');
        if (content && content.classList.contains('paywall')) {
          content.classList.remove('paywall');
          window.location.reload(true);
        }
        let full_text = document.querySelector('div.paywall:not([id])');
        if (content && full_text) {
          content.innerText = '';
          let parser = new DOMParser();
          let html = parser.parseFromString('<div>' + full_text.innerHTML + '</div>', 'text/html');
          let article = html.querySelector('div');
          content.appendChild(article);
          removeDOMElement(full_text);
          let data_adaptive = document.querySelector('div[data-adaptive="1"]');
          if (data_adaptive)
            data_adaptive.removeAttribute('data-adaptive');
          let prime_banner = document.querySelector('div.q0AQz');
          removeDOMElement(prime_banner);
        }
      }
    }, 500);
  }
}

else if (matchDomain('economictimes.indiatimes.com')) {
  let paywall = document.querySelector('section.prime_paywall');
  if (paywall) {
    removeDOMElement(paywall);
    let content = document.querySelector('div.content1, div.artText');
    let full_text = document.querySelector('div.paywall.p1');
    if (content && full_text)
      content.innerText = full_text.innerText;
    let page_content = document.querySelector('div.pageContent:not([style]');
    if (page_content)
      page_content.setAttribute('style', 'height: auto !important;');
  }
}

else if (matchDomain('economist.com')) {
  let subscribe = document.querySelector('.subscription-proposition');
  let wrapper = document.getElementById('bottom-page-wrapper');
  let adverts = document.querySelectorAll('div.advert');
  removeDOMElement(subscribe, wrapper, ...adverts);
  let p_articles = document.querySelectorAll('p.article__body-text');
  let href;
  for (let p_article of p_articles) {
    let e_anchors = document.querySelectorAll('a');
    href = '';
    for (let e_anchor of e_anchors) {
      if (e_anchor.href) {
        href = e_anchor.href;
      } else {
        e_anchor.href = href;
      }
    }
  }
}

else if (matchDomain('enotes.com')) {
  let paywall = document.querySelectorAll('section.c-cta-section');
  if (paywall.length) {
    removeDOMElement(...paywall);
    let blurred = document.querySelectorAll('div[class^="_"]');
    for (let elem of blurred)
      elem.removeAttribute('class');
    let intro = document.querySelectorAll('div.o-rte-text > p:not([class]), div.o-rte-text > h3');
    for (let elem of intro)
      removeDOMElement(elem);
    let section_words = pageContains('p[class="u-align--center"]', /\(The entire section contains/);
    let ads = document.querySelectorAll('.ad-hfu');
    removeDOMElement(...section_words, ...ads);
  }
}

else if (matchDomain('entrepreneur.com')) {
  let promo = document.querySelector('.paywall-promo');
  if (promo) {
    removeDOMElement(promo);
    let gate_check = document.querySelector('.gate-check');
    if (gate_check)
      gate_check.removeAttribute('class');
    let hidden_images = document.querySelectorAll('img.lazy[src*="blur"][data-src]');
    for (let hidden_image of hidden_images)
      hidden_image.setAttribute('src', hidden_image.getAttribute('data-src'));
  }
}

else if (matchDomain('financialexpress.com')) {
  let paywall = document.querySelector('div.paywall');
  if (paywall)
    paywall.classList.remove('paywall');
  let register = document.querySelector('div.pcl-wrap');
  let ads;
  if (window.location.pathname.endsWith('/lite/'))
    ads = document.querySelectorAll('amp-ad, amp-embed, .ad-bg-container');
  else
    ads = document.querySelectorAll('div[class*="-ads-blocks-ad-unit"]');
  removeDOMElement(register, ...ads);
}

else if (matchDomain('firstthings.com')) {//cookie
  let paywall = document.querySelector('.paywall');
  removeDOMElement(paywall);
}

else if (matchDomain('foreignaffairs.com')) {
  let paywall = document.querySelector('.paywall');
  let loading_indicator = document.querySelector('.loading-indicator');
  let msg_bottom = document.querySelector('.messages--container--bottom');
  removeDOMElement(paywall, loading_indicator, msg_bottom);
  let article_dropcap = document.querySelectorAll('.article-dropcap');
  for (let elem of article_dropcap)
    elem.classList.add('loaded');
  let hidden_images = document.querySelectorAll('img[src^="data:image/"][data-src]');
  for (let hidden_image of hidden_images) {
    hidden_image.setAttribute('src', hidden_image.getAttribute('data-src'));
    hidden_image.removeAttribute('class');
  }
  let img_list = document.querySelectorAll('.magazine-list-article img');
  for (let img_elem of img_list)
    img_elem.setAttribute('class', 'mb-4');
  if (window.location.href.includes('/interviews/')) {
    let img_header = document.querySelector('.interview-header > div');
    if (img_header) {
      let img_src = img_header.getAttribute('data-src');
      let img_elem = document.createElement('img');
      img_elem.src = img_src;
      img_header.appendChild(img_elem);
    }
  }
}

else if (matchDomain('foreignpolicy.com')) {
  let content_ungated = document.querySelector('div.content-ungated');
  removeDOMElement(content_ungated);
  let content_gated = document.querySelector('div.content-gated');
  if (content_gated)
    content_gated.classList.remove('content-gated');
}

else if (matchDomain('fortune.com')) {
  let paywall = document.querySelector('.paywall');
  if (window.location.pathname.match(/\/amp(\/)?/)) {
    amp_unhide_access_hide('="NOT p.showRegWall AND NOT p.showPayWall"', '', '[class^="amp-ad"]');
    removeDOMElement(paywall);
  } else {
    if (paywall)
      paywall.removeAttribute('class');
  }
}

else if (matchDomain('griffithreview.com')) {
  let body_single = document.querySelector('body.single');
  if (body_single)
    body_single.classList.remove('single');
  let subscribe = document.querySelector('div.call-to-action');
  removeDOMElement(subscribe);
}

else if (matchDomain('harpers.org')) {
  setCookie('hr_session', '', 'harpers.org', '/', 0);
  let overlay = document.querySelector('div[id^="pum-"]');
  removeDOMElement(overlay);
  let entry_content = document.querySelectorAll('.entry-content');
  for (let elem of entry_content)
    elem.setAttribute('style', 'display: block !important');
}

else if (matchDomain('hbr.org')) {//cookie
  let popup = document.querySelector('.persistent-banner');
  removeDOMElement(popup);
}

else if (matchDomain('hbrchina.org')) {
  let div_hidden = document.querySelector('div#the_content');
  if (div_hidden)
    div_hidden.removeAttribute('style');
}

else if (matchDomain('hilltimes.com')) {
  let paywall = document.querySelector('div.paywallcont');
  if (paywall) {
    removeDOMElement(paywall);
    let json_script = getArticleJsonScript();
    if (json_script) {
      let json = JSON.parse(json_script.text).filter(x => x.articleBody)[0];
      if (json) {
        let json_text = json.articleBody.replace(/&nbsp;/g, '').replace(/(\.|\%)\s{3,}/g, "$&\r\n\r\n");
        let content = document.querySelector('div#xorg');
        if (json_text && content)
          content.innerText = '\r\n' + json_text;
      }
    }
  }
}

else if (matchDomain('hindustantimes.com')) {
  let paywall = document.querySelector('.freemium-card');
  if (paywall) {
    removeDOMElement(paywall);
    let freemium_text = document.querySelector('.freemiumText');
    if (freemium_text)
      freemium_text.classList.remove('freemiumText');
  }
  let noscroll = document.querySelector('body.open-popup');
  if (noscroll)
    noscroll.classList.remove('open-popup');
  let close_story = document.querySelector('.closeStory');
  let ads = document.querySelectorAll('div[class^="adHeight"]');
  removeDOMElement(close_story, ...ads);
}

else if (matchDomain('historyextra.com')) {
  let article_masked = document.querySelector('.template-article__masked');
  if (article_masked) {
    let extra_pars = document.querySelectorAll('div.template-article__masked > p');
    removeDOMElement(...extra_pars);
    article_masked.classList.remove('template-article__masked');
  }
  let ad_banner = document.querySelector('.ad-banner-container');
  removeDOMElement(ad_banner);
}

else if (matchDomain(usa_hearst_comm_domains)) {
  let wrapper = document.querySelector('.belowMastheadWrapper');
  removeDOMElement(wrapper);
}

else if (matchDomain('inc42.com')) {
  let url = window.location.href;
  if (!url.includes('/amp/')) {
    let premium = document.querySelector('div.premium-container');
    if (premium) {
      removeDOMElement(premium);
      window.location.href = url.split('?')[0] + 'amp/';
    }
  } else {
    let plus_popup = document.querySelector('div#plus-pop');
    if (plus_popup) {
      removeDOMElement(plus_popup);
      let expired = document.querySelectorAll('div[amp-access="p.showPageviewExpired"], div[amp-access="cm.maxViews AND NOT loggedIn"]');
      removeDOMElement(...expired);
      amp_unhide_access_hide('^="NOT p.showPageviewExpired"')
    }
  }
}

else if (matchDomain('indianexpress.com')) {
  if (window.location.pathname.endsWith('/lite/'))
    amp_unhide_access_hide('="metering.result=\'ALLOW_ACCESS\'"', '', 'amp-ad, amp-embed');
  else {
    let paywall = document.querySelector('div#pcl-rest-content[style]');
    if (paywall)
      paywall.removeAttribute('style');
    let register = document.querySelector('div#app-pcl');
    let ads = document.querySelectorAll('div[class^="adsbox"]');
    removeDOMElement(register, ...ads);
  }
}

else if (matchDomain('indiatoday.in')) {
  if (window.location.pathname.match(/(\/amp)?\/magazine\//)) {
    let url = window.location.href;
    if (!url.includes('/amp/')) {
      let paywall = document.querySelector('#csc-paywall');
      let amphtml = document.querySelector('link[rel="amphtml"]');
      if (paywall && amphtml) {
        removeDOMElement(paywall);
        window.location.href = amphtml.href;
      }
    } else {
      amp_unhide_access_hide('="granted"', '="NOT NOT granted"', 'amp-ad, amp-embed');
    }
  }
}

else if (matchDomain('infzm.com')) {
  let url = window.location.href;
  if (url.includes('/wap/#/')) {
    let container = document.querySelector('section.container');
    if (container)
      container.classList.remove('container');
    let overlay = document.querySelector('div.article-content[style]');
    if (overlay)
      overlay.removeAttribute('style');
  } else if (url.includes('.com/contents/')) {
    window.setTimeout(function () {
      window.location.href = url.replace('.com/contents/', '.com/wap/#/content/');
    }, 500);
  }
}

if (matchDomain('inkl.com')) {
  let url = window.location.href;
  if (url.includes('/signin?') && url.includes('redirect_to=')) {
    window.setTimeout(function () {
      window.location.href = 'https://www.inkl.com' + decodeURIComponent(url.split('redirect_to=')[1]);
    }, 500);
  } else {
    let menu_btn = document.querySelector('div.left-buttons-container button.menu-btn');
    if (!menu_btn) {
      let article_container = document.querySelector('div.article-content-container');
      if (article_container) {
        article_container.setAttribute('style', 'overflow: visible; max-height: none;');
        let figures = document.querySelectorAll('figure');
        for (let figure of figures)
          figure.setAttribute('style', 'display:block !important;');
      }
      let gradient_container = document.querySelector('div.gradient-container');
      if (gradient_container)
        gradient_container.setAttribute('style', 'height:auto;');
      let locked = document.querySelector('div.locked');
      if (locked)
        locked.classList.remove('locked');
    }
    let what_is_inkl = document.querySelector('.what-is-inkl-container, .features-panel');
    let signup = document.querySelector('.article-signup-container, .locked-sign-up-container');
    removeDOMElement(what_is_inkl, signup);
    let dismiss_button = document.querySelector('div.dismiss-button-container button.btn');
    if (dismiss_button)
      dismiss_button.click();
    let shared_banner = document.querySelector('div.shared-article-inline-banner');
    removeDOMElement(shared_banner);
    let dive_deeper_summary_bodies = document.querySelectorAll('div.dive-deeper-container div.summary-body');
    if (dive_deeper_summary_bodies) {
      for (let summary_body of dive_deeper_summary_bodies) {
        if (!summary_body.querySelector('a')) {
          let ng_click = summary_body.getAttribute('ng-click').replace("showArticle('", '').replace("')", '');
          let weblink = document.createElement('a');
          weblink.text = 'open';
          weblink.href = 'https://www.inkl.com/news/' + ng_click;
          summary_body.appendChild(weblink);
        }
      }
    }
  }
}

else if (matchDomain('ipolitics.ca')) {
  let login = document.querySelector('div.login');
  if (login) {
    removeDOMElement(login);
    let json_script = document.querySelector('script#__NEXT_DATA__');
    if (json_script) {
      let json = JSON.parse(json_script.innerText);
      if (json && json.props.pageProps.post.content) {
        let article_new = json.props.pageProps.post.content;
        let article = document.querySelector('.post-header');
        if (article) {
          let parser = new DOMParser();
          let doc = parser.parseFromString('<div>' + article_new + '</div>', 'text/html');
          let content_new = doc.querySelector('div');
          article.appendChild(content_new);
        }
      }
    }
  }
}

else if (matchDomain('jpost.com')) {
  let premium_banners = document.querySelectorAll('.hide-for-premium, #hiddenPremiumForm, #hiddenLink');
  removeDOMElement(...premium_banners);
}

else if (matchDomain(['latimes.com', 'sandiegouniontribune.com'])) {
  if (window.location.search.startsWith('?_amp=true')) {
    amp_unhide_subscr_section('amp-ad, [class*="-ad-wrapper"]');
  } else {
    window.setTimeout(function () {
      let metering_bottompanel = document.querySelector('metering-bottompanel');
      removeDOMElement(metering_bottompanel);
    }, 500);
  }
}

else if (matchDomain('livelaw.in')) {
  let paywall = document.querySelector('div#subscription_paid_message, div.subscribeNow');
  if (paywall) {
    let intro = document.querySelector('div.story');
    removeDOMElement(paywall, intro);
    let restricted_message = document.querySelector('div.restricted_message');
    if (restricted_message)
      restricted_message.classList.remove('restricted_message');
    let paywall_content = document.querySelector('div.paywall-content.hide');
    if (paywall_content)
      paywall_content.classList.remove('hide');
  }
  let ads = document.querySelectorAll('inside-post-ad, amp-ad');
  removeDOMElement(...ads);
}

else if (matchDomain('livemint.com')) {
  if (window.location.pathname.includes('/amp-')) {
    let paywall = document.querySelectorAll('[amp-access="NOT subscribed"]');
    removeDOMElement(...paywall);
  }
}

else if (matchDomain('magazine.atavist.com')) {
  window.localStorage.clear();
  let bottom_notification = document.querySelector('div.bottom-notification');
  let overlay = document.querySelector('div.notification-overlay');
  removeDOMElement(bottom_notification, overlay);
  let paywall = document.querySelector('body.paywall-notification-visible');
  if (paywall)
    paywall.classList.remove('paywall-notification-visible');
}

else if (matchDomain('marketwatch.com')) {
  let premium = document.querySelector('html.is-paywall');
  let url = window.location.href;
  if (!url.includes('/amp/')) {
    if (premium) {
      premium.classList.remove('is-paywall');
      window.location.href = url.replace('.marketwatch.com/', '.marketwatch.com/amp/');
    }
  } else {
    let meter = document.querySelector('div.meter');
    let container_sponsored = document.querySelector('div.container--sponsored');
    removeDOMElement(meter, container_sponsored);
    amp_unhide_subscr_section('.display-ad');
  }
  let ads = document.querySelectorAll('div.element--ad, div.j-ad');
  removeDOMElement(...ads);
}

else if (matchDomain('medianama.com')) {
  window.setTimeout(function () {
    let modal = document.querySelector('div.modal');
    removeDOMElement(modal);
  }, 500);
  function medianama_height(node) {
    node.removeAttribute('style');
  }
  waitDOMAttribute('div.zox-post-body', 'DIV', 'style', medianama_height, true);
}

else if (matchDomain('mid-day.com')) {
  if (window.location.pathname.startsWith('/amp/')) {
    amp_unhide_access_hide('="granted"', '="NOT granted"', 'amp-ad, amp-embed, [class*="BannerAd"]');
  } else {
    let paywall = document.querySelector('div#widget-_csc');
    let amphtml = document.querySelector('link[rel="amphtml"]');
    if (paywall && amphtml) {
      removeDOMElement(paywall);
      window.location.href = amphtml.href;
    } else {
      let read_more = document.querySelector('#read-more-my');
      if (read_more)
        read_more.click();
    }
  }
}

else if (matchDomain('nation.africa')) {
  let datawall_content = document.querySelector('.datawall-content');
  if (datawall_content)
    datawall_content.classList.remove('datawall-content');
  let div_hidden = document.querySelectorAll('[data="datawall-content"]');
  for (let elem of div_hidden)
    elem.removeAttribute('style');
  let hidden_images = document.querySelectorAll('img.lazy-img:not([src])[data-srcset]');
  for (let hidden_image of hidden_images) {
    hidden_image.classList.remove('lazy-img');
    hidden_image.setAttribute('src', hidden_image.getAttribute('data-srcset').split(',')[1].split(' ')[0]);
  }
}

else if (matchDomain('nationalgeographic.com')) {
  function natgeo_func(node) {
    removeDOMElement(node);
    let body = document.querySelector('body[class]');
    if (body) {
      body.removeAttribute('class');
      body.removeAttribute('style');
    }
  }
  waitDOMElement('div[id^="fittPortal"]', 'DIV', natgeo_func, false);
  let url = window.location.href;
  let subscribed = document.querySelector('.Article__Content--gated');
  let overlay = document.querySelector('.Article__Content__Overlay--gated');
  let msg = document.querySelector('div#bpc_archive');
  if (subscribed && !msg) {
    subscribed.appendChild(archiveLink(url));
    subscribed.setAttribute('style', 'overflow: visible !important;');
    if (overlay)
      overlay.classList.remove('Article__Content__Overlay--gated');
  }
  let ads = document.querySelectorAll('div.ad-slot');
  removeDOMElement(...ads);
}

else if (matchDomain('nationalreview.com')) {
  let url = window.location.href.split('?')[0];
  if (!url.includes('/amp/')) {
    let continue_reading = document.querySelector('div.continue-reading');
    if (continue_reading) {
      removeDOMElement(continue_reading);
      window.location.href = url + 'amp';
    }
  }
  let adverts = document.querySelectorAll('amp-ad, .ad-unit--center, amp-connatix-player');
  removeDOMElement(...adverts);
}

else if (matchDomain('newleftreview.org')) {
  window.setTimeout(function () {
    let url = window.location.href;
    let paywall = document.querySelector('div.promo-wrapper');
    if (paywall) {
      removeDOMElement(paywall);
      let url_cache = 'https://webcache.googleusercontent.com/search?q=cache:' + url;
      replaceDomElementExt(url_cache, true, false, 'div.article-page');
    }
  }, 500);
}

else if (matchDomain('newrepublic.com')) {
  let pw_popups = document.querySelector('div#pwPopups');
  let ads = document.querySelectorAll('.ad-unit, .ad-container');
  removeDOMElement(pw_popups, ...ads);
}

else if (matchDomain('newsday.com')) {
  if (window.location.pathname.startsWith('/amp/')) {
    amp_unhide_access_hide('="AccessLevel = \'Full Content Access\' OR Error = true"', '="AccessLevel = \'Page View Limit\'"');
  }
}

else if (matchDomain(['nola.com', 'theadvocate.com'])) {
  if (window.location.pathname.endsWith('.amp.html')) {
    let body_hidden = document.querySelector('.site-container');
    if (body_hidden)
      body_hidden.setAttribute('style', 'display:block;');
  }
}

else if (matchDomain('nybooks.com')) {
  let paywall_article = document.querySelector('.paywall-article');
  if (paywall_article)
    paywall_article.classList.remove('paywall-article');
  let banner = document.querySelector('div.toast-cta, div.inline-ad');
  removeDOMElement(banner);
}

else if (matchDomain('nytimes.com')) {
  function nyt_main() {
    navigator.storage.estimate = undefined;
    webkitRequestFileSystem = function () {};
  }
  insert_script(nyt_main);
  let preview_button = document.querySelector('.css-3s1ce0');
  if (preview_button)
    preview_button.click();
  if (window.location.hostname === 'cooking.nytimes.com') {
    let no_scroll = document.querySelectorAll('.nytc---modal-window---noScroll');
    for (let elem of no_scroll)
      elem.classList.remove('nytc---modal-window---noScroll');
    let login = document.querySelector('div[class*="modal_modal-window-container"]:not([style="display:none;"])');
    if (login) {
      let close_button = login.querySelector('span[aria-label="close"]');
      if (!close_button)
        login.style = 'display:none;';
    }
  } else {
    waitDOMElement('div[data-testid="inline-message"]', 'DIV', removeDOMElement, false);
    waitDOMElement('div.expanded-dock', 'DIV', removeDOMElement, false);
  }
}

else if (matchDomain('nzherald.co.nz')) {
  let article_content = document.querySelector('.article__content');
  if (article_content) {
    let premium = document.querySelector('span.ellipsis');
    if (premium) {
      premium.classList.remove('ellipsis');
      let article_offer = document.querySelector('.article-offer');
      removeDOMElement(article_offer);
      let css_selector = article_content.querySelectorAll('p[style]')[1].getAttribute('class');
      let hidden_not_pars = article_content.querySelectorAll('.' + css_selector + ':not(p)');
      for (let hidden_not_par of hidden_not_pars) {
        hidden_not_par.classList.remove(css_selector);
        hidden_not_par.removeAttribute('style');
      }
      let hidden_pars = article_content.querySelectorAll('p.' + css_selector);
      let par_html, par_dom;
      let parser = new DOMParser();
      for (let hidden_par of hidden_pars) {
        let par_html = parser.parseFromString('<div style="margin: 10px 0px; font-size: 17px">' + hidden_par.innerHTML + '</div>', 'text/html');
        let par_dom = par_html.querySelector('div');
        article_content.insertBefore(par_dom, hidden_par);
      }
      let first_span = document.querySelector('p > span');
      if (first_span)
        first_span.removeAttribute('class');
    }
  }
  let premium_toaster = document.querySelector('#premium-toaster');
  removeDOMElement(premium_toaster);
}

else if (matchDomain('outlookbusiness.com')) {
  let paywall = document.querySelector('div#csc-paywall');
  if (paywall) {
    removeDOMElement(paywall);
    let json_script = document.querySelector('script#__NEXT_DATA__');
    if (json_script) {
      let json = JSON.parse(json_script.innerText);
      if (json && json.props.initialState.dashboard.ARTICLE_POST_DETAIL_API.data.article_data.description) {
        let article_new = json.props.initialState.dashboard.ARTICLE_POST_DETAIL_API.data.article_data.description;
        let article = document.querySelector('div.story-content');
        if (article) {
          article.innerHTML = '';
          let parser = new DOMParser();
          let doc = parser.parseFromString('<div>' + article_new + '</div>', 'text/html');
          let content_new = doc.querySelector('div');
          article.appendChild(content_new);
        }
      }
    }
  }
}

else if (matchDomain('outlookindia.com')) {
  let paywall = document.querySelector('div.paywall');
  if (paywall) {
    removeDOMElement(paywall);
    let json_script = getArticleJsonScript();
    if (json_script) {
      let json = JSON.parse(json_script.text);
      if (json) {
        let json_text = parseHtmlEntities(json.articleBody).replace(/\n/g, "$&\r\n");
        let content = document.querySelector('div#articleBody');
        if (json_text && content) {
          content.innerHTML = '';
          let article_new = document.createElement('p');
          article_new.innerText = json_text;
          content.appendChild(article_new);
        }
      }
    }
  }
}

else if (matchDomain('puck.news')) {
  let paywall = document.querySelector('.paywall');
  if (paywall) {
    removeDOMElement(paywall);
    let overlay = document.querySelector('body.paywall-active');
    if (overlay)
      overlay.classList.remove('paywall-active');
    let article_style = document.querySelector('article[style]');
    if (article_style)
      article_style.removeAttribute('style');
  }
}

else if (matchDomain('quora.com')) {
  let overlays = document.querySelectorAll('div[class*="_overlay"]');
  removeDOMElement(...overlays);
  let mask_image = document.querySelector('div.ePDXbR');
  if (mask_image)
    mask_image.classList.remove('ePDXbR');
}

else if (matchDomain('qz.com')) {
  if (window.location.pathname.startsWith('/emails/')) {
    let paywall = document.querySelector('div#email-content[class]');
    if (paywall) {
      paywall.removeAttribute('class');
      let login = pageContains('h2[class]', /^This story is exclusive to/);
      removeDOMElement(login[0].parentElement);
      let noscroll = document.querySelector('iframe[scrolling]');
      if (noscroll)
        noscroll.removeAttribute('scrolling');
    }
  }
}

else if (matchDomain('rugbypass.com')) {
  if (window.location.pathname.startsWith('/plus/')) {
    let paywall = document.querySelector('.premium-fold-bottom');
    if (paywall) {
      paywall.classList.remove('premium-fold-bottom');
      let offer = document.querySelector('.plus-article-offer');
      removeDOMElement(offer);
      let fade = document.querySelector('.fade');
      if (fade)
        fade.classList.remove('fade');
    }
  }
}

else if (matchDomain('science.org')) {
  let paywall = document.querySelector('div.alert-read-limit');
  removeDOMElement(paywall);
  let overlay = document.querySelector('body.alert-read-limit__overlay');
  if (overlay)
    overlay.classList.remove('alert-read-limit__overlay');
}

else if (matchDomain('scmp.com')) {
  if (window.location.href.includes('/amp.')) {
    let div_hidden = document.querySelectorAll('div.article-body[amp-access][amp-access-hide]');
    for (let elem of div_hidden)
      elem.removeAttribute('amp-access-hide');
    let default_meters = document.querySelectorAll('div.default-meter, div#archive-article-meter');
    let adverts = document.querySelectorAll('amp-ad, div.ad-banner, div.advert-fly-carpet-container, div.inline-advert');
    removeDOMElement(...default_meters, ...adverts);
  }
}

else if (matchDomain('seekingalpha.com')) {
  let url = window.location.href;
  let locked = document.querySelector('div[data-test-id="post-locked-banner"]');
  if (locked && !url.includes('/amp/')) {
    window.setTimeout(function () {
      window.location.href = url.replace('seekingalpha.com/', 'seekingalpha.com/amp/');
    }, 500);
  } else if (url.includes('/amp/')) {
    amp_unhide_access_hide('*="premium_access OR"', '', '.ad-wrap');
    let paywall = document.querySelector('[class*="paywall-container"]');
    removeDOMElement(paywall);
  }
}

else if (matchDomain('slate.com')) {
  let slate_roadblock = document.querySelector('.slate-roadblock');
  let ads = document.querySelectorAll('section[class*="-ad"]');
  removeDOMElement(slate_roadblock, ...ads);
}

else if (matchDomain('slideshare.net')) {
  window.localStorage.clear();
  let limit_overlay = document.querySelector('.limit-overlay');
  if (limit_overlay)
    limit_overlay.classList.remove('limit-overlay');
}

else if (matchDomain('sloanreview.mit.edu')) {
  let paywall = document.querySelector('div.paywall-blinder');
  removeDOMElement(paywall);
  if (paywall) {
    ext_api.runtime.sendMessage({request: 'refreshCurrentTab'});
    csDoneOnce = true;
  }
}

else if (matchDomain('sofrep.com')) {
  setCookie('sofrep_news_ids', '', 'sofrep.com', '/', 0);
  if (!window.location.pathname.startsWith('/amp/')) {
    let paywall = document.querySelector('div.fader');
    let amphtml = document.querySelector('link[rel="amphtml"]');
    if (paywall && amphtml) {
      removeDOMElement(paywall);
      window.location.href = amphtml.href;
    }
  }
  let banners = document.querySelectorAll('#scrollerCTA, #botCta');
  removeDOMElement(...banners);
}

else if (matchDomain('spglobal.com')) {
  let overlay = document.querySelector('.article__overlay');
  removeDOMElement(overlay);
  let html_noscroll = document.querySelector('html[class]');
  if (html_noscroll)
    html_noscroll.removeAttribute('class');
}

else if (matchDomain('staradvertiser.com')) {
  let url = window.location.href.split('?')[0];
  if (url.endsWith('/amp/')) {
    amp_unhide_subscr_section();
  } else {
    let paywall = document.querySelector('div#hsa-paywall-overlay');
    if (paywall) {
      removeDOMElement(paywall);
      let div_hidden = document.querySelector('div#hsa-paywall-content[style]');
      if (div_hidden)
        div_hidden.removeAttribute('style');
    }
  }
}

else if (matchDomain('startribune.com')) {
  let ads = document.querySelectorAll('div.ad-placeholder');
  removeDOMElement(...ads);
}

else if (matchDomain('stocknews.com')) {
  let hideme = document.querySelector('div#hideme');
  removeDOMElement(hideme);
  let blurmes = document.querySelectorAll('div[id^="blurme"]');
  for (let i = 0; i < blurmes.length; i++)
    blurmes[i].setAttribute('id', 'blurmenot' + i);
}

else if (matchDomain('stratfor.com')) {//articles bingbot
  let banner = document.querySelector('.free-cta-container, .paywall-banner');
  removeDOMElement(banner);
  let hidden_images = document.querySelectorAll('img[src^="data:image/gif"][data-src]');
  for (let hidden_image of hidden_images)
    hidden_image.setAttribute('src', hidden_image.getAttribute('data-src'));
  let url = window.location.href.split('?')[0];
  if (url.match(/(\/(\d){4}-([a-z]|-)+-forecast(-([a-z]|-)+)?|-forecast-(\d){4}-([a-z]|[0-9]|-)+)$/)) {
    let json_script = document.querySelector('script#__NEXT_DATA__');
    if (json_script) {
      let json = JSON.parse(json_script.innerText);
      if (json && json.props.pageProps.data) {
        let overview_div = document.querySelector('div[class^="overview_overview__"] > div');
        if (overview_div) {
          let data = json.props.pageProps.data;
          let parser = new DOMParser();
          let data_overview = data.overview;
          if (!parseHtmlEntities(data_overview).includes(data.teaser_body))
            data_overview = '<p>' + data.teaser_body + '</p>' + data_overview;
          let doc = parser.parseFromString('<div>' + data_overview + '<p><h2>Sections</h2></p>' + '</div>', 'text/html');
          let content_new = doc.querySelector('div');
          let sections = data.section;
          for (let section of sections) {
            let section_link = document.createElement('a');
            section_link.innerText = section.title;
            section_link.href = 'https://' + window.location.hostname + '/' + section.path_alias;
            content_new.appendChild(section_link);
            content_new.appendChild(document.createElement('br'));
          }
          overview_div.parentNode.replaceChild(content_new, overview_div);
        }
      }
    }
    waitDOMElement('div.paywall-banner', 'DIV', removeDOMElement, false);
  } else if (url.match(/\/article\/.+-forecast(-.+)?\//)) {
    let next_section_buttons = document.querySelectorAll('div[class^="nextSection_nextSection__"] > button');
    for (let elem of next_section_buttons) {
      let section_link = document.createElement('a');
      section_link.innerText = elem.innerText;
      section_link.href = url.replace(/[^\/]+$/, '') + elem.innerText.split(': ')[1].toLowerCase().split(' ').filter(x => !['a', 'an', 'of', 'the'].includes(x)).join('-');
      elem.parentNode.replaceChild(section_link, elem);
    }
  }
}

else if (matchDomain('techinasia.com')) {
  let paywall = document.querySelector('.paywall-content');
  if (paywall) {
    paywall.classList.remove('paywall-content');
    let url = window.location.href;
    let url_xhr = url.replace('.com/', '.com/wp-json/techinasia/2.0/posts/').replace('/visual-story/', '/');
    fetch(url_xhr)
    .then(response => {
      if (response.ok) {
        response.json().then(json => {
          let json_text = json.posts[0].content;
          json_text = json_text.replace(/width\=\"(\d){3,}\"/g, 'width="100%"').replace(/height\=\"(\d){3,}\"/g, 'height="100%"');
          let content = document.querySelector('div.content');
          if (json_text && content) {
            let parser = new DOMParser();
            let doc = parser.parseFromString('<div class="jsx-1794864983 content">' + json_text + '</div>', 'text/html');
            let content_new = doc.querySelector('div.content');
            content.parentNode.replaceChild(content_new, content);
          }
        });
      }
    });
  }
  let splash_subscribe = document.querySelector('.splash-subscribe');
  let paywall_hard = document.querySelector('.paywall-hard');
  removeDOMElement(splash_subscribe, paywall_hard);
}

else if (matchDomain('the-american-interest.com')) {
  let counter = document.getElementById('article-counter');
  removeDOMElement(counter);
}

else if (matchDomain('theathletic.com')) {
  if (!window.location.href.includes('?amp')) {
    let paywall = document.querySelectorAll('div#paywall-container, div[subscriptions-action="subscribe"], a.headline-paywall');
    let amphtml = document.querySelector('link[rel="amphtml"]');
    if (paywall.length && amphtml) {
      removeDOMElement(...paywall);
      window.setTimeout(function () {
        window.location.href = amphtml.href;
      }, 500);
    }
  } else {
    amp_unhide_subscr_section();
    let subscr_actions = document.querySelectorAll('[subscriptions-actions]');
    removeDOMElement(...subscr_actions);
    let podcast = document.querySelector('div[id^="podcast-clip-"]');
    if (podcast) {
      let podcast_src = podcast.innerHTML.replace(/<amp-/g, '<').replace(/<\/amp-/g, '</');
      let parser = new DOMParser();
      let doc = parser.parseFromString('<div>' + podcast_src + '</div>', 'text/html');
      let podcast_new = doc.querySelector('div');
      if (podcast_new)
        podcast.parentNode.replaceChild(podcast_new, podcast);
    }
  }
}

else if (matchDomain('theatlantic.com')) {
  setCookie('articleViews', '', 'theatlantic.com', '/', 0);
  let banner = document.querySelector('.c-nudge__container,.c-non-metered-nudge');
  removeDOMElement(banner);
}

else if (matchDomain('thedailybeast.com')) {
  let paywall = document.querySelector('div.Body__paywall-container');
  if (paywall) {
    removeDOMElement(paywall);
    let json_script = document.querySelector('script[displayName="initialState"]');
    if (json_script) {
      let json_str = json_script.text.substring(json_script.textContent.indexOf('{'));
      try {
        let json = JSON.parse(json_str);
        if (json.body) {
          let pars = json.body.sections;
          let cards = json.body.cards;
          if (pars) {
            let mobile_doc = document.querySelector('div.Mobiledoc');
            if (mobile_doc) {
              let mobile_doc_text = mobile_doc.innerText.replace(/(\r|\n)/g, '');
              for (let elem of pars) {
                let par_elem = '';
                if (elem[0] === 1) {
                  if (elem[1] === 'p') {
                    let par = '';
                    for (let part of elem[2])
                      par += part[3];
                    if (par && !mobile_doc_text.includes(par)) {
                      par_elem = document.createElement('p');
                      par_elem.innerText = par;
                    }
                  }
                } else if (elem[0] === 10) {
                  if (cards && cards[elem[1]]) {
                    let card = cards[elem[1]];
                    if (card[0] === 'pt-image') {
                      par_elem = document.createElement('p');
                      let par_fig = document.createElement('figure');
                      let par_img = document.createElement('img');
                      par_img.src = card[1].url;
                      par_fig.appendChild(par_img);
                      par_elem.appendChild(par_fig);
                      let par_cap = document.createElement('figcaption');
                      par_cap.innerText = card[1].title + ' ' + card[1].credit;
                      par_elem.appendChild(par_cap);
                    } else if (card[0] === 'pt-fancy-links-card') {
                      par_elem = document.createElement('p');
                      let par_link = document.createElement('a');
                      par_link.href = card[1].links;
                      par_link.innerText = card[1].linksData[0].long_headline;
                      par_elem.appendChild(par_link);
                    }
                  }
                }
                if (par_elem)
                  mobile_doc.appendChild(par_elem);
              }
            }
          }
        }
      } catch (err) {
        console.log(err);
      }
    }
  }
}

else if (matchDomain('thediplomat.com')) {
  setCookie('dpl-pw', '', 'thediplomat.com', '/', 0);
  let preview = document.querySelector('.dpl-preview');
  if (preview)
    preview.classList.remove('dpl-preview');
}

else if (matchDomain('theglobeandmail.com')) {
  let article_body_subscribed = document.querySelector('.c-article-body--subscribed');
  if (article_body_subscribed)
    article_body_subscribed.removeAttribute('class');
  let banners = document.querySelectorAll('div.c-ad, div#subscription-pencil-area, div.marketing-container-wrapper');
  removeDOMElement(...banners);
}

else if (matchDomain(['thehindu.com', 'thehindubusinessline.com'])) {
  if (!localStorage.geo) {
    localStorage.setItem("geo", '{"v":{"clientTcpRtt":20,"longitude":"' + makeRandomNumber(2) + '.' + makeRandomNumber(5) + '","httpProtocol":"HTTP/2","tlsCipher":"AEAD-AES128-GCM-SHA256","continent":"EU","asn":1234,"clientAcceptEncoding":"gzip, deflate,br","country":"UK","isEUCountry":"1","tlsClientAuth":{"certIssuerDNLegacy":"","certIssuerDN":"","certIssuerDNRFC2253":"","certSubjectDNLegacy":"","certVerified":"NONE","certNotAfter":"","certSubjectDN":"","certFingerprintSHA1":"","certNotBefore":"","certSerial":"","certPresented":"0","certSubjectDNRFC2253":""},"tlsVersion":"TLSv1.3","colo":"DUS","timezone":"Europe/London","edgeRequestKeepAliveStatus":1,"requestPriority":"weight=220;exclusive=1","botManagement":{"staticResource":false,"verifiedBot":false,"score":99},"clientTrustScore":99,"postalCode":"' + makeRandomNumber(4) + '","regionCode":"QR","region":"County","city":"London","latitude":"' + makeRandomNumber(2) + '.' + makeRandomNumber(5) + '"},"e":' + makeRandomNumber(13) + '}');
  }
  let counter = document.querySelector('#test');
  removeDOMElement(counter);
  function hindu_main() {
    document.addEventListener('bpc_event', function (e) {
      if (window) {
        window.Adblock = false;
        window.isNonSubcribed = false;
      }
    })
  }
  insert_script(hindu_main);
  document.dispatchEvent(new CustomEvent('bpc_event', {}));
}

else if (matchDomain('thenewatlantis.com')) {
  let article_gated = document.querySelector('.article-gated');
  if (article_gated)
    article_gated.classList.remove('article-gated');
}

else if (matchDomain('thepointmag.com')) {
  setCookie('monthly_history', '', 'thepointmag.com', '/', 0);
  let overlay = document.querySelectorAll('div.overlay, div#tpopup-');
  for (let elem of overlay)
    removeDOMElement(elem);
}

else if (matchDomain('thewrap.com')) {
  let paywall = document.querySelector('.wrappro-paywall');
  if (paywall)
    paywall.classList.remove('wrappro-paywall');
}

else if (matchDomain('time.com')) {
  let body = document.querySelector('body');
  if (body)
    body.setAttribute('style', 'position:relative !important;');
}

else if (matchDomain('timeshighereducation.com')) {
  let paywall_cta = document.querySelector('div.paywall-cta');
  if (paywall_cta) {
    paywall_cta.removeAttribute('style');
    let hidden_divs = document.querySelectorAll('div[style="display: none;"]');
    for (let hidden_div of hidden_divs)
      hidden_div.removeAttribute('style');
    let paywall_fade = document.querySelector('div.paywall-fade');
    if (paywall_fade)
      paywall_fade.classList.remove('paywall-fade');
  }
  let hidden_images = document.querySelectorAll('img.b-lazy[src^="data:image/"][data-src]');
  for (let hidden_image of hidden_images) {
    hidden_image.setAttribute('src', hidden_image.getAttribute('data-src'));
    hidden_image.classList.remove('b-lazy');
    hidden_image.parentElement.classList.remove('media--loading');
  }
  let ads = document.querySelectorAll('div[id^="div-gpt-in-article-ad-"], div[class^="the-dfp__in-article-ATD"]');
  removeDOMElement(...ads);
}

else if (matchDomain(no_nhst_media_domains)) {
  let url = window.location.href;
  if (url.includes('.tradewinds.com/markets/')) {
    let paywall = document.querySelector('iframe[src]');
    removeDOMElement(paywall);
    let overflow = document.querySelector('body[style]');
    if (overflow)
      overflow.removeAttribute('style');
    let blurred = document.querySelector('body > div[style]');
    if (blurred)
      blurred.removeAttribute('style');
  } else {
    window.setTimeout(function () {
      let paywall = document.querySelector('iframe#paywall-iframe');
      if (paywall) {
        let article = paywall.parentNode;
        removeDOMElement(paywall);
        fetch(url)
        .then(response => {
          if (response.ok) {
            response.text().then(html => {
              let split1 = html.split('window.__INITIAL_STATE__=')[1];
              let state = (split1.split('};')[0] + '}').split('</script>')[0];
              if (state) {
                let json = JSON.parse(state);
                if (json) {
                  let json_text = json.article.body;
                  let parser = new DOMParser();
                  let doc = parser.parseFromString('<div>' + json_text + '</div>', 'text/html');
                  let article_new = doc.querySelector('div');
                  if (article_new) {
                    if (article)
                      article.appendChild(article_new);
                  }
                }
              }
            })
          }
        })
      }
    }, 500);
  }
}

else if (domain = matchDomain(usa_conde_nast_domains)) {
  setCookie('pay_ent_smp', '', domain, '/', 0);
  setCookie('pay_ent_usmp', '', domain, '/', 0);
  if (window.location.pathname.endsWith('/amp')) {
    amp_unhide_subscr_section('amp-ad, amp-embed, .ad');
  } else {
    let paywall_bar = document.querySelector('.paywall-bar');
    removeDOMElement(paywall_bar);
  }
}

else if (matchDomain(usa_craincomm_domains)) {
  let body_hidden = document.querySelector('body[class]');
  if (body_hidden)
    body_hidden.removeAttribute('class');
  let lazy_images = document.querySelectorAll('img.lazy[data-src]');
  for (let lazy_image of lazy_images) {
    lazy_image.src = lazy_image.getAttribute('data-src');
    lazy_image.removeAttribute('class');
  }
  let lazy_sources = document.querySelectorAll('source[srcset^="data:image"]');
  removeDOMElement(...lazy_sources);
}

else if (matchDomain(usa_outside_mag_domains)) {
  window.localStorage.clear();
  let paywall = document.querySelector('div.o-membership-overlay');
  if (paywall) {
    let is_gated = document.querySelectorAll('[class*="is-gated"]');
    for (let elem of is_gated)
      removeClassesByPrefix(elem, 'is-gated');
    removeDOMElement(paywall);
  }
  if (matchDomain('cyclingtips.com')) {
    let ads = document.querySelectorAll('div[data-block-name="ads"], div#takeover');
    removeDOMElement(...ads);
  }
}

else if (matchDomain(usa_tribune_domains)) {
  let overlay = document.querySelector('div#zephr-overlay');
  removeDOMElement(overlay);
}

else if (matchDomain('usatoday.com')) {
  if (window.location.hostname.startsWith('amp.')) {
    amp_unhide_access_hide('="gup.hasAssetAccess"', '', 'div[class*="ad-"]');
  } else {
    let paywall = document.querySelector('div.gnt_rb');
    let amphtml = document.querySelector('link[rel="amphtml"]');
    if (paywall && amphtml) {
      removeDOMElement(paywall);
      window.location.href = amphtml.href;
    }
    let roadblock = document.querySelector('.roadblock-container');
    if (roadblock) {
      removeDOMElement(roadblock);
      article_next = document.querySelector('article.next-in-depth-story > div.article-inner');
      if (article_next) {
        let url = article_next.getAttribute('data-url');
        let weblink = document.createElement('a');
        weblink.href = url;
        weblink.innerText = 'open next in-depth story';
        article_next.appendChild(weblink);
      }
    }
  }
}

else if (matchDomain('venturebeat.com')) {
  let paywall = document.querySelector('div.paywall');
  if (paywall)
    paywall.classList.remove('paywall');
}

else if (matchDomain(usa_lee_ent_domains)) {
  if (window.location.pathname.endsWith('.amp.html'))
    amp_unhide_access_hide('="hasAccess"', '="NOT hasAccess"');
}

else if (domain = matchDomain(usa_mcc_domains)) {
  let url = window.location.href;
  if (url.includes('account.' + domain + '/paywall/')) {
    window.setTimeout(function () {
      window.location.href = 'https://amp.' + domain + '/article' + url.split('resume=')[1].split(/[#&]/)[0] + '.html';
    }, 500);
  } else if (url.includes('amp.' + domain + '/')) {
    amp_unhide_subscr_section();
    let subscr_tag = document.querySelector('div#subscriber-exclusive-tag');
    let amp_players = document.querySelectorAll('amp-connatix-player');
    removeDOMElement(subscr_tag, ...amp_players);
  }
  let premium_svgs = document.querySelectorAll('h3 > a > svg');
  let premium_link;
  for (let premium_svg of premium_svgs) {
    premium_link = premium_svg.parentElement;
    if (premium_link.href.includes('www.'))
      premium_link.href = premium_link.href.replace('www.', 'amp.');
  }
}

else if (domain = matchDomain(usa_mng_domains)) {
  let url = window.location.href;
  if (url.split('?')[0].endsWith('/amp/')) {
    amp_unhide_subscr_section('amp-ad, amp-embed');
  }
}

else if (matchDomain('voguebusiness.com')) {
  if (window.location.pathname.endsWith('/amp')) {
    amp_unhide_subscr_section();
  } else {
    let paywall = document.querySelector('div[class*="PaywallInlineBarrier"]');
    let amphtml = document.querySelector('link[rel="amphtml"]');
    if (paywall && amphtml) {
      removeDOMElement(paywall);
      window.location.href = amphtml.href;
    }
  }
}

else if (matchDomain('washingtonpost.com')) {
  let leaderboard = document.querySelector('#leaderboard-wrapper');
  let adverts = document.querySelectorAll('div[data-qa$="-ad"]');
  removeDOMElement(leaderboard, ...adverts);
}

else if (matchDomain('winnipegfreepress.com')) {
  let paywall = document.querySelector('.paidaccess');
  if (paywall) {
    paywall.classList.remove('paidaccess');
    let teaser = document.querySelector('#paywall-teaser');
    removeDOMElement(teaser);
  }
  let ads = document.querySelectorAll('.billboard-ad-space, .article-ad, .fixed-sky');
  removeDOMElement(...ads);
}

else if (matchDomain('wsj.com')) {
  let url = window.location.href;
  if (location.href.includes('/articles/')) {
    let close_button = document.querySelector('div.close-btn[role="button"]');
    if (close_button)
      close_button.click();
  }
  let wsj_ads = document.querySelectorAll('div[class*="wsj-ad"]');
  removeDOMElement(...wsj_ads);
  if (url.includes('/amp/')) {
    let masthead_link = document.querySelector('div.masthead > a[href*="/articles/"]');
    if (masthead_link)
      masthead_link.href = 'https://www.wsj.com';
    amp_unhide_subscr_section();
    let login = document.querySelector('div.login-section-container');
    removeDOMElement(login);
    let amp_images = document.querySelectorAll('amp-img');
    for (let amp_img of amp_images) {
      let img_new = document.createElement('img');
      img_new.src = amp_img.getAttribute('src');
      amp_img.parentNode.replaceChild(img_new, amp_img);
    }
  } else {
    let snippet = document.querySelector('.snippet-promotion, div#cx-snippet-overlay');
    let wsj_pro = document.querySelector('meta[name="page.site"][content="wsjpro"]');
    if (snippet || wsj_pro) {
      removeDOMElement(snippet, wsj_pro);
      window.location.href = url.replace('wsj.com', 'wsj.com/amp');
    }
  }
}

}

}, 1000);

// General Functions

function matchDomain(domains, hostname) {
  var matched_domain = false;
  if (!hostname)
    hostname = window.location.hostname;
  if (typeof domains === 'string')
    domains = [domains];
  domains.some(domain => (hostname === domain || hostname.endsWith('.' + domain)) && (matched_domain = domain));
  return matched_domain;
}

function setCookie(name, value, domain, path, days) {
  window.localStorage.clear();
  var max_age = days * 24 * 60 * 60;
  document.cookie = name + "=" + (value || "") + "; domain=" + domain + "; path=" + path + "; max-age=" + max_age;
}

function cookieExists(name) {
  return document.cookie.split(';').some(function (item) {
    return item.trim().indexOf(name + '=') === 0
  })
}

function removeDOMElement(...elements) {
  for (let element of elements) {
    if (element)
      element.remove();
  }
}

function waitDOMElement(selector, tagName = '', callback, multiple = false) {
  new window.MutationObserver(function (mutations) {
    for (let mutation of mutations) {
      for (let node of mutation.addedNodes) {
        if (!tagName || (node.tagName === tagName)) {
          if (node.matches(selector)) {
            callback(node);
            if (!multiple)
              this.disconnect();
          }
        }
      }
    }
  }).observe(document, {
    subtree: true,
    childList: true
  });
}

function waitDOMAttribute(selector, tagName = '', attributeName = '', callback, multiple = false) {
  let targetNode = document.querySelector(selector);
  if (!targetNode)
    return;
  new window.MutationObserver(function (mutations) {
    for (let mutation of mutations) {
      if (mutation.target.attributes[attributeName]) {
        callback(mutation.target);
        if (!multiple)
          this.disconnect();
      }
    }
  }).observe(targetNode, {
    attributes: true,
    attributeFilter: [attributeName]
  });
}

function parseHtmlEntities(encodedString) {
  let translate_re = /&(nbsp|amp|quot|lt|gt|deg|hellip|laquo|raquo|ldquo|rdquo|lsquo|rsquo|mdash|shy);/g;
  let translate = {"nbsp": " ", "amp": "&", "quot": "\"", "lt": "<", "gt": ">", "deg": "°", "hellip": "…",
      "laquo": "«", "raquo": "»", "ldquo": "“", "rdquo": "”", "lsquo": "‘", "rsquo": "’", "mdash": "—", "shy": ""};
  return encodedString.replace(translate_re, function (match, entity) {
      return translate[entity];
  }).replace(/&#(\d+);/gi, function (match, numStr) {
      let num = parseInt(numStr, 10);
      return String.fromCharCode(num);
  });
}

function replaceDomElementExt(url, proxy, base64, selector, text_fail = '', selector_source = selector) {
  let proxyurl = proxy ? 'https://bpc-cors-anywhere.herokuapp.com/' : '';
  fetch(proxyurl + url, {headers: {"Content-Type": "text/plain", "X-Requested-With": "XMLHttpRequest"} })
  .then(response => {
    let article = document.querySelector(selector);
    if (response.ok) {
      response.text().then(html => {
        if (base64) {
          html = decode_utf8(atob(html));
          selector_source = 'body';
        }
        let parser = new DOMParser();
        let doc = parser.parseFromString(html, 'text/html');
        let article_new = doc.querySelector(selector_source);
        if (article_new) {
          if (article && article.parentNode)
            article.parentNode.replaceChild(article_new, article);
        }
      });
    } else {
      if (!text_fail) {
        if (url.includes('webcache.googleusercontent.com'))
          text_fail = 'BPC > failed to load from Google webcache: '
      }
      if (text_fail && article) {
        let text_fail_div = document.createElement('div');
        text_fail_div.setAttribute('style', 'margin: 0px 50px; font-weight: bold; color: red;');
        text_fail_div.appendChild(document.createTextNode(text_fail));
        if (proxy) {
          let a_link = document.createElement('a');
          a_link.innerText = url;
          a_link.href = url;
          a_link.target = '_blank';
          text_fail_div.appendChild(a_link);
        }
        article.insertBefore(text_fail_div, article.firstChild);
      }
    }
  });
}

function archiveLink(url) {
  let text_fail_div = document.createElement('div');
  text_fail_div.id = 'bpc_archive';
  text_fail_div.setAttribute('style', 'margin: 20px; font-weight: bold; color:red;');
  text_fail_div.appendChild(document.createTextNode('BPC > Full article text:\r\n'));
  function add_links(domains) {
    for (let domain of domains) {
      let a_link = document.createElement('a');
      a_link.innerText = domain;
      a_link.href = 'https://' + domain + '?run=1&url=' + url.split('?')[0];
      a_link.target = '_blank';
      text_fail_div.appendChild(document.createTextNode(' | '));
      text_fail_div.appendChild(a_link);
    }
  }
  add_links(['archive.today', 'archive.is']);
  return text_fail_div;
}

function amp_iframes_replace(weblink = false, source = '') {
  let amp_iframes = document.querySelectorAll('amp-iframe' + (source ? '[src*="'+ source + '"]' : ''));
  let elem;
  for (let amp_iframe of amp_iframes) {
    if (!weblink) {
      elem = document.createElement('iframe');
      Object.assign(elem, {
        src: amp_iframe.getAttribute('src'),
        sandbox: amp_iframe.getAttribute('sandbox'),
        height: amp_iframe.getAttribute('height'),
        width: 'auto',
        style: 'border: 0px;'
      });
      amp_iframe.parentElement.insertBefore(elem, amp_iframe);
      removeDOMElement(amp_iframe);
    } else {
      let video_link = document.querySelector('a#bpc_video_link');
      if (!video_link) {
        amp_iframe.removeAttribute('class');
        elem = document.createElement('a');
        elem.id = 'bpc_video_link';
        elem.innerText = 'Video-link';
        elem.setAttribute('href', amp_iframe.getAttribute('src'));
        elem.setAttribute('target', '_blank');
        amp_iframe.parentElement.insertBefore(elem, amp_iframe);
      }
    }
  }
}

function amp_unhide_subscr_section(amp_ads_sel = 'amp-ad, .ad', replace_iframes = true, amp_iframe_link = false, source = '') {
  let preview = document.querySelector('[subscriptions-section="content-not-granted"]');
  removeDOMElement(preview);
  let subscr_section = document.querySelectorAll('[subscriptions-section="content"]');
  for (let elem of subscr_section)
    elem.removeAttribute('subscriptions-section');
  let amp_ads = document.querySelectorAll(amp_ads_sel);
  removeDOMElement(...amp_ads);
  if (replace_iframes)
    amp_iframes_replace(amp_iframe_link, source);
}

function amp_unhide_access_hide(amp_access = '', amp_access_not = '', amp_ads_sel = 'amp-ad, .ad', replace_iframes = true, amp_iframe_link = false, source = '') {
  let access_hide = document.querySelectorAll('[amp-access' + amp_access + '][amp-access-hide]:not([amp-access="error"], [amp-access^="message"])');
  for (let elem of access_hide)
    elem.removeAttribute('amp-access-hide');
  if (amp_access_not) {
    let amp_access_not_dom = document.querySelectorAll('[amp-access' + amp_access_not + ']');
    removeDOMElement(...amp_access_not_dom);
  }
  let amp_ads = document.querySelectorAll(amp_ads_sel);
  removeDOMElement(...amp_ads);
  if (replace_iframes)
    amp_iframes_replace(amp_iframe_link, source);
}

function insert_script(func, insertAfterDom) {
  let bpc_script = document.querySelector('script#bpc_script');
  if (!bpc_script) {
    let script = document.createElement('script');
    script.setAttribute('id', 'bpc_script');
    script.appendChild(document.createTextNode('(' + func + ')();'));
    let insertAfter = insertAfterDom ? insertAfterDom : (document.body || document.head || document.documentElement);
    insertAfter.appendChild(script);
  }
}

function getArticleJsonScript() {
  let scripts = document.querySelectorAll('script[type="application/ld+json"]');
  let json_script;
  for (let script of scripts) {
    if (script.innerText.includes('articleBody')) {
      json_script = script;
      break;
    }
  }
  return json_script;
}

})();
